/**
 * @file
 * D3 Sparkarea library.
 */

(function ($) {
  'use strict';

  Drupal.d3.sparkarea = function (select, settings) {
    var data = (settings.rows.map(function (d) {
        return {
          x: new Date(d[0]),
          y: d.slice(1).map(function (n) {
            return +n;
          })
        };
      })),
      stack = d3.stack().keys(Array.from(settings.rows[0].keys()).slice(1)),
      series = stack(settings.rows),
      width = /%$/.test(settings.width) ? parseInt(settings.width, 10) / 100 * document.getElementById(settings.id).clientWidth : settings.width,
      // Inset by 2 to prevent clipping.
      insetLeft = settings.insetLeft || (settings.axis_y_show ? 25 : 2),
      insetRight = settings.insetRight || (settings.labels === undefined ? 15 : 35),
      insetBottom = settings.insetBottom || (settings.axis_x_show ? 20 : 2),
      x = d3.scaleUtc()
        .domain([data[0].x, data[data.length - 1].x])
        .range([insetLeft, width - insetRight]),
      y = d3.scaleLinear()
        .domain([0, d3.max(data.map(function (d) { return d3.sum(d.y); }))])
        .range([settings.height - insetBottom, settings.insetTop || 2]),
      graph = d3.select('#' + settings.id)
        .append('svg').attr('class', 'sparkarea').attr('width', width).attr('height', settings.height)
        .append('g').attr('class', 'chart'),
      last = settings.height,
      labelHeight = 12,
      extraSpace = 0,
      seriesLabelMod = 10,
      g, labelLines;

    // Calculate positions of labels, and lines connecting to the graph.
    if (settings.labels !== undefined) {
      labelLines = series.map(function (s, n) {
        // Start point, midpoint (average) of the last data point’s top &
        // bottom.
        var line = [[0, y(d3.mean(s[s.length - 1]))]];
        // Line area is 15px wide, 2px separation from the label. If the label
        // is less than labelHeight from the last label, push it to avoid
        // overlap.
        line.push([15 - 2, d3.min([line[0][1], last - labelHeight])]);
        extraSpace += last - line[1][1] - labelHeight;
        last = line[1][1];

        return line;
      });
      if (last < 0) {
        // Ideally push off-canvas labels back in, the labels are vertically
        // centered on the line end. Only use available space, if limited.
        extraSpace = d3.min([-last + labelHeight / 2, extraSpace]);
        labelLines = labelLines.reverse().map(function (line, n) {
          if (n > 0) {
            extraSpace -= line[1][1] - last - labelHeight;
          }
          last = line[1][1];
          line[1][1] += d3.max([extraSpace, 0]);

          return line;
        }).reverse();
      }
    }

    series.map(function (s, n) {
      // Area graph.
      graph.append('path')
        .attr('class', 'area series-' + (n % seriesLabelMod))
        .attr('d', (d3.area()
          .x(function(d, i) { return x(data[i].x); })
          .y0(function(d) { return y(d[0]); })
          .y1(function(d) { return y(d[1]); })
        )(s));

      if (labelLines !== undefined) {
        graph.append('path')
          .attr('class', 'label-line series-' + (n % seriesLabelMod))
          .attr('transform', 'translate(' + (width - insetRight) + ', 0)')
          .attr('d', (d3.line())(labelLines[n]));
        graph.append('text')
          .attr('class', 'label-text series-' + (n % seriesLabelMod))
          .attr('dominant-baseline', 'middle')
          .attr('transform', 'translate(' + (width - insetRight + 15) + ', ' + labelLines[n][1][1] + ')')
          .text(settings.labels[n]);
      }
    });

    // X axis.
    if (settings.axis_x_show) {
      g = graph.append('g')
        .attr('transform', 'translate(0,' + (settings.height - insetBottom) + ')')
        .call(d3.axisBottom(x).ticks(Math.min(5, settings.rows.length)));
      g.select('.domain').remove();
    }

    // Y axis.
    if (settings.axis_y_show) {
      g = graph.append('g')
        .attr('transform', 'translate(' + insetLeft + ', 0)')
        .call(d3.axisRight(y).ticks(5, 's').tickSize(width - insetLeft - insetRight));
      g.selectAll('.tick text').attr('text-anchor', 'end').attr('x', -2);
      g.select('.tick:first-of-type').remove();
      g.select('.domain').remove();
    }
  };
})(jQuery);
