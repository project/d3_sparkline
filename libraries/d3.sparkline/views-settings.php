<?php

function d3_library_sparkline_views_settings() {
  return [
    'width' => [
      '__form_element' => [
        'title' => t('Width'),
        'default_value' => 100,
      ],
    ],
    'height' => [
      '__form_element' => [
        'title' => t('Height'),
        'default_value' => 20,
      ],
    ],
    'axis_x_show' => [
      '__form_element' => [
        'title' => t('Show X axis'),
        'type' => 'checkbox',
      ],
    ],
    'axis_y_show' => [
      '__form_element' => [
        'title' => t('Show Y axis'),
        'type' => 'checkbox',
      ],
    ],
  ];
}
