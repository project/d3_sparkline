/**
 * @file
 * D3 Sparkline library.
 */

(function ($) {
  Drupal.d3.sparkline = function (select, settings) {
    var data = (settings.rows.map(function (d) {
        return {
          x: new Date(d[0]),
          y: d.slice(1).map(function (n) {
            return +n;
          })
        };
      })),
      // Inset by 2 to prevent clipping.
      x = d3.scaleUtc()
        .domain([data[0].x, data[data.length - 1].x])
        .range([(settings.axis_y_show ? 25 : 2), settings.width - 2]),
      y = d3.scaleLinear()
        .domain([0, d3.max(data.map(function (d) { return d3.max(d.y); }))])
        .range([settings.height - (settings.axis_x_show ? 20 : 2), 2]),
      graph = d3.select('#' + settings.id)
        .append('svg').attr('class', 'sparkline').attr('width', settings.width).attr('height', settings.height)
        .append('g').attr('class', 'chart'),
      g;

    data[data.length - 1].y.map(function (last, k) {
      // Line
      graph.append('path')
        .attr('class', 'line')
        .attr('d', (d3.line()
          .x(function (d) { return x(d.x); })
          .y(function (d) { return y(d.y[k]); })
        )(data));

      // Dot
      graph.append('circle')
        .attr('class', 'end')
        .attr('cx', settings.width - 2)
        .attr('cy', y(last))
        .attr('r', 2);

      // End value
      $('#' + settings.id).append('<span class="end-value">' + last + '</span>');
    });

    // X axis.
    if (settings.axis_x_show) {
      g = graph.append('g')
        .attr('transform', 'translate(0,' + (settings.height - 20) + ')')
        .call(d3.axisBottom(x));
      g.select('.domain').remove();
    }

    // Y axis.
    if (settings.axis_y_show) {
      g = graph.append('g')
        .attr('transform', 'translate(25,0)')
        .call(d3.axisRight(y).ticks(5, 's').tickSize(settings.width - 25));
      g.selectAll('.tick text').attr('text-anchor', 'end').attr('x', -2);
      g.select('.tick:first-of-type').remove();
      g.select('.domain').remove();
    }
  };
})(jQuery);
